import firebrowse
import json
import urllib
import tarfile
import csv
import sys
import argparse
import os
import gzip
import shutil

parser = argparse.ArgumentParser(description='ICAro ')

parser.add_argument('--gene', required=True,help='deleted gene to mine signature.')
parser.add_argument('--dataset', required=True,help='TCGA cohort, full list available at https://gdac.broadinstitute.org/')
parser.add_argument('--output', required=True,help='output folder')
parser.add_argument('--focality', default=0, required=False,help='tolerance kbs to mine CNVs from. ')
parser.add_argument('--type', required=False,default="del",help='CNV type, amp or del (default: "del")')
parser.add_argument('--profile', required=False,default="profile.txt",help='Molecular txt profile file, in Onco Query Language (OQL) ')
parser.add_argument('--inac_mut', required=False,default="",help='Inactivating mutation list file. Required if sample_selection is 2')
parser.add_argument('--sample_selection', required=False,default=1,help='type of selection for samples data: 1) focal CNVs and possibly inactivating mutations; 2) only inactivating mutations, not considering CNVs"')

args = parser.parse_args()

sample_selection = int(args.sample_selection)

if sample_selection == 2:
    parser.error("--inac_mut is required if --sample_selection is 2")
    sys.exit(1)

gene=args.gene
dataset=args.dataset
out_dir=args.output
focality=int(args.focality)
cnv_type=args.type
profile_file=args.profile
file_name_inactivating_mutations=args.inac_mut
data_file_dir = "data"
mutations_dir = os.path.join(data_file_dir, "mutations")

cnv_tool = "Merge_snp__genome_wide_snp_6__broad_mit_edu__Level_3__segmented_scna_hg19__seg"
rna_tool = "Merge_rnaseqv2__illuminahiseq_rnaseqv2__unc_edu__Level_3__RSEM_genes__data"


inactivating_mutation_selection = False
if file_name_inactivating_mutations != "":
    if not os.path.exists(file_name_inactivating_mutations):
        parser.error("Inactivating mutation list file does not exist")
        sys.exit(1)
    
    inactivating_mutation_selection = True


# TODO fetch patient number and calculate minimal cardinality for GEP computing
patient_count = 20

# absolute gistic score for amp-deletechars
score = 1

annotation_file = "annotation/hg19_gene_coordinates.bed"

gene_chr = ""

threshold_cnv_segment_size_for_control = 1000000
#threshold_cnv_segment_size_for_control = float("inf")
min_cnv_size = 0

with open(annotation_file, "rb") as csvfile:
    datareader = csv.DictReader(csvfile, delimiter='\t')
    count = 0
    for row in datareader:
        if row["gene"] == gene and "PATCH" not in row["chr"]:
            gene_chr = row["chr"]
            gene_start = int(row["start"])
            gene_end = int(row["end"])
            print gene,"Coordinates found: chr",gene_chr,gene_start,gene_end;
            break
	  
if gene_chr == "":
  print "Sorry, couldn't find gene '"+gene+"' in hg19 annotation."
  sys.exit(1)

profile_list = dict()

with open(profile_file, "rb") as csvfile:
        datareader = csv.DictReader(csvfile, delimiter='\t')
        for row in datareader:
            split = row.split(":")
            profile_list[split[1]] = split[0] 
         

if not os.path.exists(out_dir):
    os.makedirs(out_dir)
    
if not os.path.exists(data_file_dir):
    os.makedirs(data_file_dir)
    
if not os.path.exists(mutations_dir):
    os.makedirs(mutations_dir)

cnv_file_name = os.path.join(data_file_dir,"CNV_" + dataset + ".txt.gz")

if not os.path.exists(cnv_file_name):
    filename_cnv = os.path.join(data_file_dir,dataset+"_gep_"+".tar.gz")

    # germline subtracted variants is way lower.. when compared to cbioportal signal 
    # convinced that the cbioportal data is without germline subtraction

    c = firebrowse.Archives().StandardData(format="json", cohort=dataset, data_type="CopyNumber", page_size="100", tool=cnv_tool, level="3")

    json_data = json.loads(c)["StandardData"]

    # TODO automatize/customize FFPE vs FF data fetch
    # first element has FFPE data, second (1) has fresh frozen data
    default_type = "frozen"
    for i_json in range(0,len(json_data)):
        if json.loads(c)["StandardData"][i_json]['sample_prep'] == default_type:
            cnv_url = json.loads(c)["StandardData"][i_json]['urls'][0]
            break
    
    #l = len(json_data)
    #fetch = len(json_data) - 1
    #fetch = i_json

    #cnv_url = json.loads(c)["StandardData"][fetch]['urls'][0]
    urllib.urlretrieve(cnv_url, filename_cnv)

    tar = tarfile.open(filename_cnv)
    n = tar.getnames()
    tar.extractall(path=data_file_dir)
    tar.close()
    os.remove(filename_cnv)

    seg_txt_file_name = os.path.join(data_file_dir, filter(lambda x:'seg.txt' in x, n)[0])
    
    with open (seg_txt_file_name, "rb") as file_in, gzip.open(cnv_file_name, "wb") as file_out:
        shutil.copyfileobj(file_in, file_out)
        

#overlapping_cnv_count=0
patient_ids = set()

patients_excluded_for_cnv_filter = set()
patients_to_include = set()

with gzip.open(cnv_file_name, 'rb') as csvfile:
  cnv_reader = csv.DictReader(csvfile, delimiter='\t', quotechar='|')
  for row in cnv_reader:
    patient = row["Sample"]
    
    cnv_start = float(row["Start"])
    cnv_end = float(row["End"])
    cnv_chr = row["Chromosome"]
    cnv_size = cnv_end - cnv_start + 1
    cnv_signal = float(row["Segment_Mean"])
    focality_fix = focality
#    if(patient not in patient_ids and ((cnv_st art - focality_fix) < gene_end and gene_start < (cnv_end + focality_fix)) and cnv_chr == gene_chr and cnv_signal <= -2):

    gene_start  = gene_start - focality_fix
    gene_end = gene_end + focality_fix
      
    included_in_samples = False
    excluded_from_samples = False
    

    if abs(cnv_signal) > score:           
        if((cnv_start) < gene_end and gene_start < (cnv_end) and (cnv_chr == gene_chr)) and ((cnv_type == "del" and cnv_signal <= -1*score) or (cnv_type == "amp" and cnv_signal >= score)):
            if cnv_size >= min_cnv_size:
                included_in_samples = True

        if not included_in_samples:
            if cnv_size > threshold_cnv_segment_size_for_control:
                excluded_from_samples = True
            
    if included_in_samples:
        if sample_selection == 1: #if sample selection is not for only inactivating mutations. It stays here because it has to not include samples with cnvs in the selected gene and to not exclude samples with cnvs in this region
            if patient not in patient_ids:

                    patient_ids.add(patient)
                    #overlapping_cnv_count = overlapping_cnv_count + 1
        
    elif excluded_from_samples:
        patients_excluded_for_cnv_filter.add(patient)
        
    else:
        patients_to_include.add(patient)


patients_total_control = patients_to_include - patients_excluded_for_cnv_filter - patient_ids

file_name_mutation_profile = os.path.join(mutations_dir, "mutations_" + dataset + "_" + gene + ".tsv")

only_cnv_samples = len(patient_ids)

if not os.path.exists(file_name_mutation_profile):
    urllib.urlretrieve("http://firebrowse.org/api/v1/Analyses/Mutation/MAF?format=tsv&cohort=" + dataset + "&tool=MutSig2CV&gene=" + gene + "&page=1&page_size=250&sort_by=cohort", file_name_mutation_profile)

if inactivating_mutation_selection:
    list_inactivating_mutations=[]
    max_codon_position_for_nonsense = 100
    #max_codon_position_for_frameshift = 100
    
    for line in open(file_name_inactivating_mutations, "r").readlines():
        lstrp = line.rstrip().rstrip(",")
        list_inactivating_mutations.append(lstrp)
    
        if lstrp[0:4] == "STOP":
            max_codon_position_for_nonsense = int(lstrp[4:].lstrip())
        
patients_excluded_for_mutation_filter = set()
with open(file_name_mutation_profile, 'rb') as csv_mp_file:
    cnv_mp_reader = csv.DictReader(csv_mp_file, delimiter='\t', quotechar='|')    

    for row in cnv_mp_reader:            
        patient_mutation = row["Tumor_Sample_Barcode"][0:12]
        
        if inactivating_mutation_selection:            
            protein_change = row["Protein_Change"][2:]
            variant_classification = row["Variant_Classification"]
            variant_type = row["Variant_Type"]
            
            
            if variant_type == "SNP":
                if variant_classification == "Missense_Mutation":
                    if protein_change in list_inactivating_mutations:
                        patient_ids.add(patient_mutation)
                
                elif variant_classification == "Nonsense_Mutation":
                    pos_cod = int(protein_change[1:-1])
                    if pos_cod <= max_codon_position_for_nonsense:
                        patient_ids.add(patient_mutation)
            
            #elif variant_type in ("INS", "DEL"):
            #    pos_cod = int(protein_change[1:-2])
            #    if pos_cod <= max_codon_position_for_frameshift:
            #        patient_ids.add(patient_mutation)
        else:
            patients_excluded_for_mutation_filter.add(patient_mutation)


patients_total_control = patients_total_control - patients_excluded_for_mutation_filter - patient_ids
                                

num_patient_ids = len(patient_ids)
#if(overlapping_cnv_count < (patient_count/20) and overlapping_cnv_count == 0):
if(num_patient_ids < (patient_count/20) and num_patient_ids == 0):
  print "Sorry, there are only "+str(num_patient_ids)+" patients with such a "+gene+" CNV, with the desidered focality."
  sys.exit(1)
else:
    if sample_selection == 1:
        if inactivating_mutation_selection:
            print str(only_cnv_samples)+" patients found with a focal CNV."
            print str(num_patient_ids - only_cnv_samples) + " patients found with at least one inactivating mutation."
            print str(num_patient_ids) + " total patients found. - Clustering..."
        else:
            print str(num_patient_ids)+" patients found with a focal CNV. - Clustering..."
    elif sample_selection == 2:
        print str(num_patient_ids) + " patients found with at least one inactivating mutation. - Clustering..."


rna_file_name = os.path.join(data_file_dir, "mRNA_" + dataset + ".txt.gz")

if not os.path.exists(rna_file_name):
    filename_gep = os.path.join(data_file_dir,dataset+"_gep_"+".tar.gz")

    # fetch RNA-seq processed data and extract tarball 
    r = firebrowse.Archives().StandardData(format="json", cohort=dataset, data_type="mRNAseq", page_size="100", protocol="RSEM_genes", tool=rna_tool, level="3")

    gep_url = json.loads(r)["StandardData"][0]['urls'][0]
    urllib.urlretrieve (gep_url, filename_gep)

    tar = tarfile.open(filename_gep)
    n = tar.getnames()
    tar.extractall(path=data_file_dir)
    tar.close()
    os.remove(filename_gep)
    
    data_txt_file_name = os.path.join(data_file_dir, filter(lambda x:'data.txt' in x, n)[0])
    
    with open (data_txt_file_name, "rb") as file_in, gzip.open(rna_file_name, "wb") as file_out:
        shutil.copyfileobj(file_in, file_out)


new_patient_ids = set()
for p_id in patient_ids:
    new_patient_ids.add(p_id[0:12])

new_patient_ids_total_control = set()
for p_id in patients_total_control:
    new_patient_ids_total_control.add(p_id[0:12])



filtered_sample_names = []
filtered_sample_control_names = []
l_isample = ["1"] #1 is for the first column (using cut command) that contains gene names

with gzip.open(rna_file_name, 'rb') as rnaFile:
    for row in rnaFile:
        rowsplt = row.strip().split("\t")
                                
        for idx, el in enumerate(rowsplt):
            if (idx % 3) == 1:
                el_red = el[0:12]
                
                if el_red in new_patient_ids:
                    filtered_sample_names.append(el)
                    l_isample.append(str(idx + 1))
                    
                elif el_red in new_patient_ids_total_control:
                    filtered_sample_control_names.append(el)
                    l_isample.append(str(idx + 1))
        break

len_filtered_sample_names = len(filtered_sample_names)
len_filtered_sample_control_names = len(filtered_sample_control_names)

if len_filtered_sample_names == 0:
    print "No mRNA inactivated samples have been found."
    sys.exit(1)
else:
    print str(len_filtered_sample_names) + " mRNA inactivated samples have been found."
    if len_filtered_sample_names < 5:
        print "At least 5 inactivated samples are required."
        sys.exit(1)

if len(filtered_sample_control_names) == 0:
    print "No mRNA control samples have been found."
    sys.exit(1)
else:  
    print str(len_filtered_sample_control_names) + " mRNA control samples have been found."
    
ratio_samples_controls = float("{0:.2}".format(1.0 * len_filtered_sample_names / len_filtered_sample_control_names))
threshold_ratio_sc = 0.05
if ratio_samples_controls <= threshold_ratio_sc:
    print "The ratio between inactivated and control samples is only " + str(ratio_samples_controls) + ". A ratio of at least " + str(threshold_ratio_sc) + " is required."
    sys.exit(1)


for file_in_data in os.listdir(data_file_dir):
    if cnv_tool in file_in_data or rna_tool in file_in_data:
        shutil.rmtree(os.path.join(data_file_dir, file_in_data))

filtered_file_name = os.path.join(out_dir, "filtered_samples_" + gene + "_" + dataset + ".txt")
filtered_file = open(filtered_file_name, "wb")
filtered_file.write("Sample_ID" + os.linesep)
for sample in filtered_sample_names:
    filtered_file.write(sample + os.linesep)
filtered_file.close()

filtered_control_file_name = os.path.join(out_dir, "filtered_negative_samples_" + gene + "_" + dataset + ".txt")
filtered_control_file = open(filtered_control_file_name, "wb")
filtered_control_file.write("Sample_ID" + os.linesep)
for sample in filtered_sample_control_names:
    filtered_control_file.write(sample + os.linesep)
filtered_control_file.close()

index_column_file_name = os.path.join(out_dir, "index_columns_" + gene + "_" + dataset + ".txt")
index_column_file = open(index_column_file_name, "wb")
index_column_file.write(",".join(l_isample))
index_column_file.close()

sys.exit(0)
