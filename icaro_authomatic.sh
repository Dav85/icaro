#/bin/sh

#$1 filename gene and dataset
#$2 output folder
#$3 user email
#$4 inactivating mutation list file

out=$2

if [ ! -f $out ]
then
    mkdir $out
fi

final_file="${out}/recap.csv"
##with edgeR
#echo "logFC,logCPM,qvalue,gene,size_test_set,size_control_set,dataset" > $final_file
#with voom
echo "gene,logFC,qvalue,ave_expression_inactivated,ave_expression_control,size_inactivated_set,size_control_set,dataset" > $final_file


while IFS=$' ' read -r -a row
do
    gene=${row[0]}
    dataset=${row[1]}

    filename_all_genes="${out}/all_genes_${gene}_${dataset}.tsv"
    echo $gene and $dataset
    
    if [ ! -f $filename_all_genes ]
    then                
        if [ "$#" -eq "3" ]            
        then
            sh icaro.sh $gene $dataset $2 $3
        else
            sh icaro.sh $gene $dataset $2 $3 $4
        fi
    fi
    
    #check twice the existence of the file because it could haven't been created due to sample missing
    if [ -f $filename_all_genes ]
    then
        gene_mod=$(echo $gene | tr "-" ".")
        
        gene_grep_count=$(grep "${gene_mod}" $filename_all_genes -w | wc -l)
        
        if [ $gene_grep_count -eq 1 ]
        then
            #with edgeR output
            #var1=$(grep "${gene_mod}" $filename_all_genes -w | cut -f 1,2,5,6 | tr '\t' ',')
            
            #with voom output
            var1=$(grep "${gene_mod}" $filename_all_genes -w | cut -f 1,2,6,8,9 | tr '\t' ',')
            size_test_set=$(expr $(cat "${out}/filtered_samples_${gene}_${dataset}.txt" | wc -l) - 1)
            size_control_set=$(expr $(cat "${out}/filtered_negative_samples_${gene}_${dataset}.txt" | wc -l) - 1)
            
            echo $var1,$size_test_set,$size_control_set,$dataset >> $final_file
        fi
    fi
done <$1