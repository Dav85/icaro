#/bin/sh
#$1 gene
#$2 dataset
#$3 output folder
#$4 user email 
#$5 inactivating mutation list file

#cd icaro

FILEPATH="./$3/all_genes_"$1"_"$2".tsv"

echo "executing python script..."

if [ "$#" -eq "4" ]
then
  python -W ignore icaro_beta.py --gene $1 --dataset $2 --output $3 > icaro_out
else
  python -W ignore icaro_beta.py --gene $1 --dataset $2 --output $3 --inac_mut $5 > icaro_out
fi

if [ $? -eq "1" ]
then
    echo "Analysis interrupted"
    icaro_log=$(cat icaro_out)
    echo "Dear User, \n\nthis is an automated message from the ICAro system. Unfortunately, the analysis couldn't be completed. This is the log message:\n\n$icaro_log\n\nKind Regards,\nthe ICAro team"  | mutt  -s  "ICAro output $1 $2" -b matteo.pallocca@gmail.com -d 1 -- $4
else	
	idx_file="$3/index_columns_$1_$2.txt"
	data_dir="data"
	gzipped_file="${data_dir}/mRNA_$2.txt.gz"
	reduced_file="$3/reduced_$1_$2.txt"

	idx=$(cat $idx_file)
	zgrep -v 'raw_count' $gzipped_file | cut -f $idx > $reduced_file

	rm $idx_file

	echo "executing R script..."
	Rscript icaro.R $1 $2 $3
	echo "Dear User, \n\nthis is an automated message from the ICAro system. You will find attached your analysis output.\n\nKind Regards,\nthe ICAro team"  | mutt -a $FILEPATH -s  "ICAro output $1 $2" -b matteo.pallocca@gmail.com -d 1 -- $4
fi


#chown www-data:www-data "data/mutations_$3_$4.tsv"
